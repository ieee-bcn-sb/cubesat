The main repo for the cubesat project. Tutorial for git:

- Español (corto): <https://www.diegocmartin.com/tutorial-git/>
- Español oficial (largo): <https://git-scm.com/book/es/v1/Empezando>

### Overleaf users

The repo is cloned in overleaf, in case you prefer to edit the documents on the
web. Please follow the link <https://es.overleaf.com/1495195923scnqtjbstqyw> and
take a look at the `doc/` folder.
