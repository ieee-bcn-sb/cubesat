## REUNIÓN GENERAL PROYECTO GRSS NANOSATÉLITE

ORDEN DEL DÍA

1. Explicar situación actual y qué queda por hacer. Compromiso del equipo.
	- Deadline de la Proposal
2. Exponer tareas por hacer y establecer su orden de importancia.
	- Enumerar y explicar brevemente los documentos que todo el equipo debería
	leer. Son de conocimiento general y esenciales para saber qué estamos
	haciendo y cómo lo haremos.
		1. Cubesat’s basics
		2. Mission requirements
		3. Spacecraft System Engineering
		4. Slides Juan Ramos

	- Escoger número de payloads. => __De momento 3__
	- Escoger qué payloads llevar a cabo. => Espectrómetro / Sensor temperatura / Radiación partículas cargadas
	- Proponer fecha para la reunión con Juan Ramos => 3-4 Septiembre
	- Proponer cargos para los integrantes del grupo.
	- __Comunicación__: Encargado de comunicarse con profes, enviar documentos y
	  organizar los recursos del equipo. Proponer reuniones y hacer doodles.
	Organizar al equipo, chequear que todo este al día.
	
	- __Administrator__: Encargado de la predicción del Budget y de buscar becas.
	- __Research__: Persona o grupo de personas encargados de buscar documentos
	y información de interés general.
	- __IEEC responsible__: Encargado de analizar y comprender el hardware que
	nos deja Juan Ramos para poner nuestra Payload. \*, **
	- __Recopilador__: Encargado/a de recopilar todo lo que hagamos y hacer un
	pequeño diario. hay 2 razones por la que quiero proponer este cargo: La
	primera es porque creo que ayuda a la organización y la segunda por que
	podría ser muy útil de cara a futuros miembros que quieran hacer proyectos
	similares.
	- __Proposal__: Encargado de que la Proposal este lista de cara a la Deadline y
	reunión con juan Ramos.
3. Repartir cargos entre todos los integrantes del grupo.
4. Proponer Deadlines para todas las tareas por hacer, PROPOSAL sobre todo.
5. Turno abierto.

\* Creo que estaría bien que una persona se lo mirara con calma y preguntara para saber de
los recursos que disponemos y para explicárselo al grupo entero de una manera más
amena. Ahorraremos tiempo.

** Hay uno/dos documentos sobre el hardware.

### Experimentos: Tenemos que meter tantos experimentos como sea posible.

- Ver composición y propiedades de la atmósfera.
- Espectómetro de 1 píxel
- Lentes para centrar. => Especie de telescopio pequeño
- Filtro
- Cámara para centrar => Centrar espectrómetro / Fotos
- Mirar filtro para poder sacar diferentes espectros con una cámara normal => Fabry-Perot + spectral filters


Medir temperatura tierra
Sensor radiación partículas cargadas

**TAREAS**

- Buscar precio de espectrómetro
- Buscar patrocinadores
- Buscar un video o pdf que explique como funciona un espectrómetro.
- Preguntar Nanosatlab contacto para tractament de los datos una vez recibidos, como
proceder a procesarlos.
- Poner antena para recibir en el espacio
- Mirar viabilidad de aprovechar el espectrómetro mientras no podamos hacer
experimento principal. consultar tema EPS, grupo compartido.
- MIrar factores varios como temperatura para las payloads. Ver si necesitamos uso
de refrigeradores/calentadores

**FASES**

- Captación de datos
Fase inicial 11/08/19
- Todo el mundo se lea lo básico sobre satélites y las payloads
- Researchers hayan algún documento sobre las payloads
- Comunicación organize los documentos / Ir buscando becas para saber "Budget general"
- IEEC responsible => Haber leído parte del documento para ayudar a Research
- Proposal => Empezar documento por ejemplo por el Background

---

Hola a todos !!!

He estado hablando con Rodrigo y hemos hecho un resumen de todas las tareas
pendientes. Para poder trabajar todos en paralelo, hemos propuesto diferentes roles para
los miembros del equipo. Cada persona debería asumir al menos uno. Los roles son:

- Comunicación
- Administrador Budget
- Research
- IEEC Responsible
- Recopilador
- Proposal responsible

Podéis encontrar la definición de cada uno de los roles en el documento “Cargos y Tareas”
de la carpeta que tenemos compartida en el Drive. Los que no estéis decidlo y os lo envio.
Apuntaros en el documento al lado de cada rol para ir sabiendo quien es quien.
Cada rol tiene unas tareas iniciales asignadas para antes de la reunión con Juan Ramos.
Rodrigo y yo hemos propuesto de volver a hacer una llamada dentro de más o menos una
semana, para revisar que todo vaya correctamente y decidir en qué parte nos hemos de
centrar más. Creemos que hacer esto es un buen método para ir al día con las tareas, se
aceptan propuestas.

Para esta revisión, también hay tareas asignadas para cada rol.
Es muuuuy importante que llevemos a cabo las deadlines que nosotros mismos nos
proponemos porque la deadline de la Proposal está casi a la vuelta de la esquina.
Apuntaros todos porfavor, si creeis que el rol que escogeis puede tener alguna otra función
diferente avisad y lo cambiamos. También podéis proponer otros roles o apuntaros a más
de uno. Pero lo más importante es avanzar trabajo de cara a la semana que viene porque
hay un montón de cosas por hacer.

Make IEEE great again !!!
